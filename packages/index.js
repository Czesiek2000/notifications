module.exports.defaultToast = function (player, message, out, remove) {
    player.call('defaultToast', message, out, remove);
}

module.exports.errorToast = function (player, message, icon, out, remove) {
    player.call('errorToast', message, icon, out, remove)
}

module.exports.successToast = function (player, message, icon, out, remove) {
    player.call('successToast', message, icon, out, remove);
}

module.exports.infoToast = function (player, message, icon, out, remove) {
    player.call('infoToast', message, icon, out, remove);
}

module.exports.warningToast = function (player, message, icon, out, remove) {
    player.call('warningToast', message, icon, out, remove);
}

module.exports.imageToast = function (player, message, icon, description, image, out, remove) {
    player.call('imageToast', message, icon, description, image, out, remove);
}

module.exports.customToast = function (player, message, icon, iconType, pos, color, shadow, out, remove) {
    player.call('customToast', message, icon, iconType, pos, color, shadow, out, remove);
}