let browser;

browser = mp.browsers.new("package://notification/ui/index.html");

mp.events.add('defaultToast', (text, out, remove) => {
    browser.execute(`defaultToast('${text}', '${out}', '${remove}')`);
})

mp.events.add('errorToast', (text, icon, out, remove) => {
    browser.execute(`errorToast('${text}', '${icon}', '${out}', '${remove}')`);
})

mp.events.add('successToast', (text, icon, out, remove) => {
    browser.execute(`successToast('${text}', '${icon}', '${out}', '${remove}')`);
})

mp.events.add('infoToast', (text, icon, out, remove) => {
    browser.execute(`infoToast('${text}', '${icon}', '${out}', '${remove}')`);
})

mp.events.add('warningToast', (text, icon, out, remove) => {
    browser.execute(`warningToast('${text}', '${icon}', '${out}', '${remove}')`);
})

mp.events.add('imageToast', (text, icon, description, image, out, remove) => {
    browser.execute(`imageToast('${text}', '${icon}', '${description}', '${image}', '${out}', '${remove}' )`);
})

mp.events.add('customToast', (text, icon, iconType, pos, color, shadow, out, remove) => {
    browser.execute(`customToast('${text}', '${icon}', '${iconType}', '${pos}', '${color}', '${shadow}', '${out}', '${remove}')`);
})