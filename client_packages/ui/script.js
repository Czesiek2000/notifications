const notification = document.querySelector('.notifyContainer');

const toast = new Toast(notification);

function defaultToast(text, out, remove) {
    toast.default(text, out, remove);
}

function errorToast(text, icon, out, remove) {
    toast.error(text, icon, out, remove);
}

function successToast(text, icon, out, remove) {
    toast.success(text, icon, out, remove);
}

function infoToast(text, icon, out, remove) {
    toast.info(text, icon, out, remove);
}

function warningToast(text, icon, out, remove) {
    toast.warning(text, icon, out, remove);
}

function imageToast(text, description, image, background, out, remove) {
    toast.image(text, description, image, background, out, remove);
}

function customToast(text, icon, iconType, pos, color, shadow, out, remove) {
    toast.custom(text, icon, iconType, pos, color, shadow, out, remove);
}