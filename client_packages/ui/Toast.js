/* 
 * Toast notification for RAGEMP or browser usage
 * MIT Licence Czesiek2000 2020 - now
 * https://gitlab.com/Czesiek2000/notifications
 */
class Toast {

    // _version = '2.0';
    constructor(notification) {
        this.container = notification;

        this.top = document.createElement('div');
        this.top.className += `top absolute `
        document.body.append(this.top);

        this.bottom = document.createElement('div');
        this.bottom.className += `bottom absolute `;
        document.body.append(this.bottom);

        this.right = document.createElement('div');
        this.right.className += `right absolute `;
        document.body.appendChild(this.right);

        this.left = document.createElement('div');
        this.left.className += `left absolute `;
        document.body.appendChild(this.left);

        this.bottomRight = document.createElement('div');
        this.bottomRight.className += `bottom-right absolute `;
        document.body.appendChild(this.bottomRight);

        this.bottomLeft = document.createElement('div');
        this.bottomLeft.className += `bottom-left absolute `;
        document.body.appendChild(this.bottomLeft);

        this.topRight = document.createElement('div');
        this.topRight.className += `top-right absolute `;
        document.body.appendChild(this.topRight);

        this.topLeft = document.createElement('div');
        this.topLeft.className += `top-left absolute `;
        document.body.appendChild(this.topLeft);
    }
    /* 
     * Creates default message
     * @param text Text that will be displayed inside notification
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */
    default (text = "This is default toast notification", out = 3500, remove = 4500) {
        // let noti = this._noti;
        let noti = document.createElement('div');
        noti.classList.add('notification');


        noti.innerText = text;
        this.container.appendChild(noti);

        this.animated(noti, out);
        this.remove(noti, remove);
    }

    /* 
     * Creates success message with green background
     * @param _text Text that will be displayed inside notification
     * @param _icon Defines if icon should appear inside toast
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */

    success(_text = "Success message", _icon = false, out = 3500, remove = 4500) {
        const noti = document.createElement('div');
        const icon = document.createElement('i');
        const text = document.createElement('div');

        noti.className += `notification icon-notification green`;
        text.innerText = _text;

        if (_icon) {
            icon.className += `fas fa-check icon-text`;
            noti.appendChild(icon);
        }

        noti.appendChild(text);
        this.container.appendChild(noti);

        this.animated(noti, out);
        this.remove(noti, remove);
    }

    /* 
     * Creates warning message
     * @param _text Text that will be displayed inside notification
     * @param _icon Defines if icon should appear inside toast
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */

    warning(_text = "Warning message", _icon = false, out = 3500, remove = 4500) {
        const noti = document.createElement('div');
        const icon = document.createElement('i');
        const text = document.createElement('div');

        noti.className += `notification icon-notification yellow`
        if (_icon) {
            icon.className += `fas fa-exclamation-triangle icon-text`
            noti.appendChild(icon);
        }

        text.innerText = _text;
        noti.appendChild(text);
        this.container.appendChild(noti);

        this.animated(noti, out);
        this.remove(noti, remove);
    }

    /* 
     * Creates error message
     * @param _text Text that will be displayed inside notification
     * @param _icon Defines if icon should appear inside toast
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */

    error(_text = "Error message", _icon = false, out = 3500, remove = 4500) {
        const noti = document.createElement('div');
        const icon = document.createElement('i');
        const text = document.createElement('div');

        noti.className += `notification icon-notification red`
        if (_icon) {
            icon.className += `fas fa-exclamation icon-text`
            noti.appendChild(icon);
        }

        text.innerText = _text;
        noti.appendChild(text);
        this.container.appendChild(noti);

        this.animated(noti, out);
        this.remove(noti, remove);
    }

    /* 
     * Creates info message
     * @param _text Text that will be displayed inside notification
     * @param _icon Defines if icon should appear inside toast
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */

    info(_text = "Success message", _icon = false, out = 3500, remove = 4500) {
        const noti = document.createElement('div');
        const icon = document.createElement('i');
        const text = document.createElement('div');

        noti.className += `notification icon-notification blue`
        if (_icon) {
            icon.className += `fas fa-info-circle icon-text`
            noti.appendChild(icon);
        }

        text.innerText = _text;
        noti.appendChild(text);
        this.container.appendChild(noti);

        this.animated(noti, out);
        this.remove(noti, remove);
    }

    /* 
     * Creates toast with image
     * @param _text Sets notification header above image
     * @param _description Sets text under image inside notification 
     * @param _image Sets url to image that will be displayed
     * @param _background Sets notification background available values (deafult, success, green, red, blue, yellow)
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */

    image(_text = "Image notification", _description = "Image notification description", _image = "image.png", _background = "default", out = 3500, remove = 4500) {
        const noti = document.createElement('div');
        const title = document.createElement('div');
        const image = document.createElement('img');
        const description = document.createElement('div');

        noti.className += `notification image-notification ` + _background;

        title.innerText = _text;
        image.src = _image;
        image.classList.add('image');
        description.innerText = _description;
        description.classList.add('noti-description');

        noti.append(title);
        noti.append(image);
        noti.append(description);

        this.container.appendChild(noti);

        this.animated(noti, out);
        this.remove(noti, remove);
    }

    /* 
     * Method which add some classes from animated css library
     * @param noti Div to which animations will be added, which is single toast
     * @param remove How fast should animated css leaving animation class be aplied to div, this parameter is passed from single toast message function
     */

    animated(noti, out) {
        noti.className += ' animate__animated' + ' animate__bounceInLeft';
        setTimeout(() => {
            noti.classList.add('animate__bounceOutRight')
        }, out);
    }

    /* 
     * Creates default message
     * @param noti Div that will be removed from notification container
     * @param remove How fast should remove from notification container, this parameter is passed from single toast message function
     */
    remove(noti, remove) {
        setTimeout(() => {
            noti.remove();
        }, remove);
    }

    /* 
     * Creates custom notification
     * @params _text Text to display
     * @params _icon Define if icon should be displayed
     * @params _iconType Set custom icon type
     * @params pos Define where to place the notification (top, bottom, left, right, topRight, topLeft, bottomRight, bottomLeft)
     * @params _color Define background color of notification (default, green, blue, red, yellow, random(set random background color)) 
     * @params _shadow Define if notification has shadow
     * @param out How fast should be added leave animation
     * @param remove How fast should remove from main container
     */
    custom(_text = "Notification title", _icon = false, _iconType = "fas fa-bell", pos = "top", _color = "default", _shadow = false, out = 3500, remove = 4500) {
        const noti = document.createElement('div');
        const icon = document.createElement('i');
        const text = document.createElement('div');

        if (_color === "random") {
            let random = Math.floor(Math.random() * 16777215).toString(16);
            noti.style.backgroundColor = `#${random}`;
        }
        noti.className += `notification icon-notification ` + _color;
        text.innerText = _text;

        if (_text.length >= 40) {
            noti.style.height = '70px';
            icon.style.paddingTop = '11px';
            text.innerText = "Text is to long, cannot render"
        }

        if (_icon) {
            icon.className += `icon-text ` + _iconType;
            noti.appendChild(icon);
        }

        noti.appendChild(text);

        switch (pos) {
            case "top":
                this.top.appendChild(noti);
                break;

            case "bottom":
                this.bottom.appendChild(noti);
                break;

            case "left":
                this.left.appendChild(noti);
                break;

            case "right":
                this.right.appendChild(noti);
                break;

            case "top-right":
                this.topRight.appendChild(noti);
                break;

            case "top-left":
                this.topLeft.appendChild(noti);
                break;

            case "bottom-right":
                this.bottomRight.appendChild(noti);
                break;

            case "bottom-left":
                this.bottomLeft.appendChild(noti);
                break;

            default:
                this.top.appendChild(noti);
                break;
        }

        this.animated(noti, out);
        this.remove(noti, remove);
    }
}