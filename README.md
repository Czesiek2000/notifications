<div align="center">
<h1>NOTIFICATION</h1></div>

This project is basic implementation of notifications using CEF build in RAGEMP. For now there are four messages that you can use as you can see in preview image. Default message place is top right side of the screen. 

## Preview
![preview](preview.png)

## Available toast
This project has 5 available toast methods which you can call with some parameters to create cool toast notification.

By default out is equls to *3500ms*(3,5 second) and remove is equls to *4500ms*(4,5s), so toast will be visible for *1000 ms - 1 second*.

* default - creates default notification with light black color.
    * text - text which will be displayed in notification
    * out - how fast should go out from screen in miliseconds
    * remove - how fast should remove from DOM in miliseconds

* success - creates success notification with light green color.
    * text - text which will be displayed in notification
    * icon - add icon - true / false -> default is false, not showing icon
    * out - how fast should go out from screen in miliseconds
    * remove - how fast should remove from DOM in miliseconds

* warning - creates warning notification with light yellow color.
    * text - text which will be displayed in notification
    * icon - add icon - true / false -> default is false, not showing icon
    * out - how fast should go out from screen in miliseconds
    * remove - how fast should remove from DOM in miliseconds

* error - creates error notification with light red color.
    * text - text which will be displayed in notification
    * icon - add icon - true / false -> default is false, not showing icon
    * out - how fast should go out from screen in miliseconds
    * remove - how fast should remove from DOM in miliseconds

* info - creates info notification with light blue color.
    * text - text which will be displayed in notification
    * icon - add icon - true / false -> default is false, not showing icon
    * out - how fast should go out from screen in miliseconds
    * remove - how fast should remove from DOM in miliseconds

* custom - creates custom notification.
    * text - text which will be displayed in notification
    * icon - add icon - true / false -> default is false, not showing icon
    * icon type - add icon from fontawesome library
    * pos - specify where to position the icon
    * color - specify color of notification
    * shadow - specify if shadow should be added to notification
    * out - how fast should go out from screen in miliseconds
    * remove - how fast should remove from DOM in miliseconds

For more specifi info about parameters check `Toast.js` file inside ui folder where all parameters are describe with more details.

You can also check live version [here](https://czesiek2000.gitlab.io/notifications/) and see how it looks before using it on you server.

If you :thumbsup: or :heart: this resource, don't forget to leave :star: on this repo or :pencil: nice comment on the [forum](https://rage.mp/forums/).

If you found any :bug: in this script, leave :thought_balloon: on the [forum](https://rage.mp/forums/) or open [issue](https://gitlab.com/Czesiek2000/teleport-to-location/-/issues) in this repo 
Thanks :punch:, enjoy this resource.